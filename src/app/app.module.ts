import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { AppComponent }        from './app.component';

import { DashboardComponent }  from '../components/dashboard/dashboard.component';
import { TeamDetailComponent } from '../components/teams/team-detail.component';
import { TeamsComponent }      from '../components/teams/teams.component';

import { TeamService }         from '../services/team.service';

import { routing } from './app.routing';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing
  ],
  declarations: [
    AppComponent,
    TeamDetailComponent,
    TeamsComponent,
    DashboardComponent
  ],
  providers: [
    TeamService
  ],
  bootstrap: [ AppComponent ]
})

export class WebGM {}
