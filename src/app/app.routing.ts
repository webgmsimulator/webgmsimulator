import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent }  from '../components/dashboard/dashboard.component';
import { TeamsComponent }      from '../components/teams/teams.component';
import { TeamDetailComponent } from '../components/teams/team-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'teams',
    component: TeamsComponent
  },
  {
    path: 'team/:id',
    component: TeamDetailComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
