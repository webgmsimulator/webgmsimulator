import { Component } from '@angular/core';

@Component({
  selector: 'web-gm',
  template: `
    <h1>{{title}}</h1>
    <nav>
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/teams" routerLinkActive="active">Teams</a>
    </nav>
    <router-outlet></router-outlet>
  `
})

export class AppComponent {
  title = 'WebGm';
}
