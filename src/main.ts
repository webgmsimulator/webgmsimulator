import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { WebGM } from './app/app.module';

const platform = platformBrowserDynamic();

platform.bootstrapModule(WebGM);
