import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Team } from '../../models/team.model';
import { TeamService } from '../../services/team.service';

@Component({
  moduleId: module.id,
  selector: 'dashboard',
  styleUrls:  ['styles/dashboard.component.css'],
  templateUrl: 'templates/dashboard.component.html',
  encapsulation: ViewEncapsulation.Native,
})

export class DashboardComponent {}
