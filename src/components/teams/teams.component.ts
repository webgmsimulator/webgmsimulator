import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Team } from '../../models/team.model';
import { TeamService } from '../../services/team.service';

@Component({
  moduleId: module.id,
  selector: 'teams',
  providers: [TeamService],
  styleUrls:  ['styles/teams.component.css'],
  templateUrl: 'templates/teams.component.html',
  encapsulation: ViewEncapsulation.Native,
})

export class TeamsComponent implements OnInit {
  teams: Team[];

  constructor(
    private router: Router,
    private teamService: TeamService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams(): void {
    this.teamService.getTeams().then(teams => this.teams = teams);
  }

  gotoDetail(team): void {
    this.router.navigate(['/team', team.id]);
  }
}
