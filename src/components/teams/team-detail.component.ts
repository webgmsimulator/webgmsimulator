// Keep the Input import for now, we'll remove it later:
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Team } from '../../models/team.model';
import { TeamService } from '../../services/team.service';

@Component({
  moduleId: module.id,
  selector: 'team-detail',
  styleUrls:  ['styles/team-detail.component.css'],
  templateUrl: 'templates/team-detail.component.html',
  encapsulation: ViewEncapsulation.Native,
})

export class TeamDetailComponent implements OnInit {
  team: Team;

  constructor(
    private teamService: TeamService,
    private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.teamService.getTeam(id)
        .then(team => this.team = team);
    });
  }

  goBack(): void {
    window.history.back();
  }
}
